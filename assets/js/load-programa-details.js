"use strict";
(function () {
    const mainContainer = document.querySelector(".main-wapper");
    const jsonPath = mainContainer.dataset.jsonPath;
    const programaId = Number(mainContainer.dataset.id);
    if (jsonPath == null) {
        throw new Error("'data-json-path' is not set");
    }
    // Loads the json data
    fetch('assets/data/programas/' + jsonPath)
        .then(res => res.json())
        .then(json => json.data)
        .then(data => {
        for (const e of data) {
            if (e.id === programaId) {
                render(e);
                break;
            }
        }
    });
    // Inject the HTML
    function render(programa) {
        console.log(programa);
        // Set page title
        document.title = programa.nombre;
        // Sets the banner values
        const courseSection = mainContainer.querySelector(".course-section");
        const ofertaHTML = mainContainer.querySelectorAll("[data-oferta]");
        const tipoHTML = mainContainer.querySelectorAll("[data-programa-tipo]");
        const nombreHTML = mainContainer.querySelectorAll("[data-programa-nombre]");
        courseSection.style.backgroundImage = `url(${programa.banner})`;
        ofertaHTML.forEach(e => e.innerText = programa.ofertaAcademica);
        tipoHTML.forEach(e => e.innerText = programa.tipo);
        nombreHTML.forEach(e => e.innerText = programa.nombre);
        // Set description
        const description = getDescription(programa);
        // Set content
        const content = getContent(programa);
        // Set teachers
        const teachers = getTeachers(programa);
        // Container
        const tabnavContainer = createNode(`<div class="container">
          <div class="tab-content text-left" id="tabnavcontent">
              ${description}
              ${content}
              ${teachers}
            </div>
          </div>`);
        mainContainer.appendChild(tabnavContainer);
        console.log(mainContainer);
    }
    function getDescription(programa) {
        return `
        <!-- Description -->
        <div class="tab-pane fade show active col-md-12 col-lg-8 pb-sm-4" id="description" role="tabpanel"
          aria-labelledby="description-tab">
          <button class="
                btn btn-custom btn-primary
                px-5
                text-sm
                w-100
                mb-4
                d-md-block d-sm-block d-lg-none
              ">
            APLICAR AHORA
          </button>

          <div class="text-left">
            <div class="course-title">
              <h3 data-programa-nombre>${programa.nombre.toUpperCase()}</h3>
            </div>
          </div>

          <section>
            <div class="course-description mb-4">
              <p>
              ${programa.descripcion || ""}
              </p>
              <p class="font-weight-bolder mb-1 text-dark">PROPÓSITO</p>
              <p>
              ${programa.proposito}
              </p>
            </div>
            <hr />
          </section>

          ${programa.metodologia && createSection("METODOLOGÍA", programa.metodologia)}

          ${programa.lugar && createSection("LUGAR", programa.lugar)}

          ${programa.horario && createSection("HORARIO", programa.horario)}

          ${programa.fechas && createSection("FECHAS", programa.fechas)}

          ${programa.modalidad && createSection("MODALIDAD", programa.modalidad)}

          <button class="btn btn-apply btn-custom btn-primary px-5 text-sm">
            APLICAR AHORA
          </button>
        </div>`;
    }
    function getContent(programa) {
        return `
      <!-- Content -->
      <div class="tab-pane fade col-md-12 col-lg-8" id="content" role="tabpanel" aria-labelledby="content-tab">
        <button class="
              btn btn-custom btn-primary
              px-5
              text-sm
              w-100
              mb-4
              d-md-block d-sm-block d-lg-none
            ">
          APLICAR AHORA
        </button>

        <div class="text-left">
          <div class="course-title">
            <h3>${programa.nombre.toUpperCase()}</h3>
          </div>
        </div>

        ${programa.contenido && createSection("CONTENIDO", programa.contenido)}

        ${programa.dirigido && createSection("PÚBLICO A CAPACITAR", programa.dirigido)}

        <button class="btn btn-apply btn-custom btn-primary px-5 text-sm">
          APLICAR AHORA
        </button>
      </div>`;
    }
    function getTeachers(programa) {
        return `<!-- Teachers -->
      <div class="course-teachers col-lg-4 col-md-12 col-sm-12">
        <button class="btn btn-custom btn-primary px-5 text-sm btn-50-100 mt-sm-4">
          APLICAR
        </button>
        <div class="section-heading d-flex align-items-center">
          <div class="heading-content">
            <h2>
              <span class="text-weight">Docentes</span>
              <span class="header-right"></span>
            </h2>
          </div>
        </div>
        
        ${programa.docentes.map(e => createTeacherCard(e)).join("\n")}
      </div>`;
    }
    function createTeacherCard(teacher) {
        return `<div class="instructor-card">
      <div class="instructor-profile align-items-center">
        <div class="instructor-img">
          <img src="${teacher.foto}" alt="${teacher.nombre}" />
        </div>
        <div class="instructor-desc">
          <h3 class="mb-0 text-warning">${teacher.nombre}</h3>
          <span class="text-primary">Docente</span>
        </div>
      </div>
      <div class="instructor-content text-left">
        <!-- <div class="ic-right">
            <a href="#"
              >Ver Perfil <i class="fas fa-caret-right right-nav"></i
            ></a>
          </div> -->
      </div>
    </div>`;
    }
    function createSection(title, content) {
        if (Array.isArray(content)) {
            return `
          <section>
            <span class="font-weight-bolder text-dark">${title}</span>
            <ul class="check-list">
              ${createList(content)}
            </ul>
            <hr />
        </section>`;
        }
        else {
            return `
            <section>
                <span class="font-weight-bolder text-dark">${title}</span>
                <p>${content}</p>
                <hr/>
          </section>`;
        }
    }
    function createNode(html) {
        const element = document.createElement("div");
        element.innerHTML = html;
        return element.firstChild;
    }
    function createElement(html) {
        const element = document.createElement("div");
        element.innerHTML = html;
        return element.firstElementChild;
    }
    function createList(content) {
        const elements = [];
        for (const e of content) {
            if (Array.isArray(e)) {
                const list = createList(e);
                elements.push(`<ul class="ml-4">${list}</ul>`);
            }
            else {
                elements.push(`<li><span>${e}</span></li>`);
            }
        }
        return elements.join('\n');
    }
})();
