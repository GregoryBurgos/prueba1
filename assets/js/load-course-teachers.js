"use strict";
(function () {
    const container = document.getElementById("docentes-container");
    if (container == null) {
        throw new Error("'#docentes-container' container not found");
    }
    // Ids
    const docentesDataIds = container.dataset.ids
        ?.split(",")
        .filter((e) => e.length !== 0)
        .map((e) => Number(e.trim()));
    // Fetch the data
    fetch("../assets/data/docentes.json")
        .then((res) => res.json())
        .then((json) => json.data)
        .then((data) => render(data));
    // Inserts the HTML
    function render(docentes) {
        if (docentesDataIds == null) {
            throw new Error(`'data-ids' it not defined in '#docente-container', you must pass an array of ids: 'data-ids: "1,3,5,7"'`);
        }
        const docentesToShow = [];
        for (const id of docentesDataIds) {
            const docente = docentes.find((e) => e.id === id);
            if (docente == null) {
                throw new Error(`Cannot find docente with id: ${id}`);
            }
            docentesToShow.push(docente);
        }
        // Sort by 'primerNombre'
        docentesToShow.sort((a, b) => {
            const primerNombreA = a.nombre;
            const primerNombreB = b.nombre;
            return primerNombreB.localeCompare(primerNombreA);
        });
        // console.log(docentesDataIds);
        for (const docente of docentesToShow) {
            const card = docenteCard(docente);
            container.insertAdjacentElement("afterbegin", card);
        }
        // 
        showCards();
    }
    function docenteCard(docente) {
        const fullName = getFullName(docente);
        const html = `
        <div class="instructor-card">
            <div class="instructor-profile align-items-center">
            <div class="instructor-img">
                <img src="../assets/images/docentes/${docente.foto}" />
            </div>
            <div class="instructor-desc">
                <h3 class="mb-0 text-warning">${fullName}</h3>
                <p class="text-primary">${docente.ocupacion}</p>
            </div>
            </div>
            <div class="instructor-content text-left"></div>
            <a class="stretched-link" href="../docentes/${docente.id}.html"></a>
      </div>`;
        const div = document.createElement("div");
        div.innerHTML = html;
        return div.firstElementChild;
    }
    function getFullName(docente) {
        return docente.honorifico
            ? `${docente.honorifico}. ${docente.nombre}`
            : docente.nombre;
    }
    async function showCards() {
        const cards = document.querySelectorAll(".instructor-card");
        const time = 100;
        for (let i = 0; i < cards.length; i++) {
            const c = cards.item(i);
            const delay = (1 + i) * time;
            setTimeout(() => c.classList.add("show"), delay);
        }
    }
})();
